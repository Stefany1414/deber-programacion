package edu.istdab.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.istdab.model.Paciente;

public interface IPacienteRepo extends JpaRepository<Paciente, Integer> {

}
