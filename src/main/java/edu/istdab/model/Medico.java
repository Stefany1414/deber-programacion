package edu.istdab.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="medico")
@PrimaryKeyJoinColumn(name="id_persona")
public class Medico extends Persona{

	@Size(min=3, message = "Consultorio debe tener minimo 3 caracteres")
	@Column(name="consultorio", nullable = false, length = 70)
	private String consultorio;
	
	@Column(name="foto_url", nullable = false, length = 70)
	private String foto_url;
	
	public String getConsultorio() {
		return consultorio;
	}

	public void setConsultorio(String consultorio) {
		this.consultorio = consultorio;
	}

	public String getFoto_url() {
		return foto_url;
	}

	public void setFoto_url(String foto_url) {
		this.foto_url = foto_url;
	}

	
}
