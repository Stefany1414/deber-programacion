package edu.istdab.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="menu")
public class Menu {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idMenu;
	
	@Size(min=3, message = "Nombres debe tener minimo 3 caracteres")
	@Column(name ="nombre", nullable = false, length = 50)
	private String nombre;
	
	@Column(name ="url", nullable = false, length = 50)
	private String url;
	
	@Size(min=1, message = "Nombres debe tener minimo 1 caracteres")
	@Column(name ="icono", nullable = false, length = 70)
	private String icono;
    
	@ManyToMany
	@JoinTable(name="menu_rol", joinColumns = @JoinColumn(name="id_menu",referencedColumnName = "idMenu"),inverseJoinColumns = @JoinColumn(name="id_rol",referencedColumnName = "idRol") )
	private List<Rol> roles;
	
	public Integer getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Integer idMenu) {
		this.idMenu = idMenu;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public List<Rol> getRoles() {
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}
	
	
}
