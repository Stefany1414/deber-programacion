package edu.istdab.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="paciente")
@PrimaryKeyJoinColumn(name="id_persona")
public class Paciente extends Persona {

	@Size(min=3, message = "historia Clinica debe tener minimo 3 caracteres")
	@Column(name="historiaClinica", nullable = false, length = 10)
	private String historiaClinica;
	
	@Size(min=10, message = "Enfermedades debe tener minimo 10 caracteres")
	@Column(name="enfermedades", nullable = false)
	private String enfermedades;

	public String getHistoriaClinica() {
		return historiaClinica;
	}

	public void setHistoriaClinica(String historiaClinica) {
		this.historiaClinica = historiaClinica;
	}

	public String getEnfermedades() {
		return enfermedades;
	}

	public void setEnfermedades(String enfermedades) {
		this.enfermedades = enfermedades;
	}
	
	
}
