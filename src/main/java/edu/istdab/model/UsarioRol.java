package edu.istdab.model;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="usuario_rol")
public class UsarioRol {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private Integer idUsuarioRol;
	
	@ManyToOne
	@JoinColumn(name="id_usuario", nullable=false, foreignKey = @ForeignKey(name="FK_UR_USUARIO"))
	private Usuario usuario;
	
	@ManyToOne
	@JoinColumn(name="id_rol", nullable=false, foreignKey = @ForeignKey(name="FK_UR_ROL"))
	private Rol rol;

	public Integer getIdUsuarioRol() {
		return idUsuarioRol;
	}

	public void setIdUsuarioRol(Integer idUsuarioRol) {
		this.idUsuarioRol = idUsuarioRol;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	
	
	
	
}
