package edu.istdab.model;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Entity
@Table(name="persona")
@Inheritance(strategy = InheritanceType.JOINED)
public class Persona {
	
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer idPersona;


@Size(min=3, message = "Nombres debe tener minimo 3 caracteres")
@Column(name="nombres", nullable = false, length = 70)
private String nombres;

@Size(min=3, message = "Apellidos debe tener minimo 3 caracteres")
@Column(name="apellidos", nullable = false, length = 70)
private String apellidos;

@Size(min=10, max=10, message = "Cedula debe tener minimo 10 caracteres")
@Column(name="cedula", nullable = false, length = 10)
private String cedula;

@Size(min=6,max=10, message = "Telefono debe tener minimo 3 caracteres")
@Column(name="telefono", nullable = false, length = 10)
private String telefono;


@Size(min=3, message = "Direccion debe tener minimo 3 caracteres")
@Column(name="direccion", nullable = false, length = 200)
private String direccion;

@Email
@Column(name="email", nullable = false, length = 70)
private String email;


@Column(name="edad", nullable = false, length = 2)
private int edad;

@Column(name="sexo", nullable = false, length = 1)
private String sexo;

@Size(min=3, message = "Tipo de Sangre debe tener minimo 3 caracteres")
@Column(name="tipoSangre", nullable = false, length = 5)
private String tipoSangre;



public Integer getIdPersona() {
	return idPersona;
}

public void setIdPersona(Integer idPersona) {
	this.idPersona = idPersona;
}

public String getNombres() {
	return nombres;
}

public void setNombres(String nombres) {
	this.nombres = nombres;
}

public String getApellidos() {
	return apellidos;
}

public void setApellidos(String apellidos) {
	this.apellidos = apellidos;
}

public String getCedula() {
	return cedula;
}

public void setCedula(String cedula) {
	this.cedula = cedula;
}

public String getTelefono() {
	return telefono;
}

public void setTelefono(String telefono) {
	this.telefono = telefono;
}

public String getDireccion() {
	return direccion;
}

public void setDireccion(String direccion) {
	this.direccion = direccion;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public int getEdad() {
	return edad;
}

public void setEdad(int edad) {
	this.edad = edad;
}

public String getSexo() {
	return sexo;
}

public void setSexo(String sexo) {
	this.sexo = sexo;
}

public String getTipoSangre() {
	return tipoSangre;
}

public void setTipoSangre(String tipoSangre) {
	this.tipoSangre = tipoSangre;
}


}
