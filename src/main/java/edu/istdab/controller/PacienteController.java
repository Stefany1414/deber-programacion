package edu.istdab.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.istdab.model.Paciente;
import edu.istdab.service.IPacienteService;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {

	@Autowired
	private IPacienteService service;
	
	@GetMapping
	public List<Paciente> listar(){
		return service.listar();
	}

	@GetMapping("/{id}")
	public Paciente listarPorId(@PathVariable("id")Integer id){
		return service.leerPorId(id);
	}
	
	@PostMapping
	public Paciente registrar(Paciente paciente) {
		return service.registrar(paciente);
	}
	
	@PutMapping
	public Paciente modificar(Paciente paciente) {
		return service.registrar(paciente);
	}
	
	@DeleteMapping
	public void eliminar(Integer id)
	{
		service.eliminar(id);
	}
	
	
}
