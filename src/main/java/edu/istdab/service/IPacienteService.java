package edu.istdab.service;

import java.util.List;

import edu.istdab.model.Paciente;

public interface IPacienteService {

	Paciente registrar(Paciente paciente);
	Paciente modificar(Paciente paciente);
	List<Paciente>listar();
	Paciente leerPorId (Integer tid);
	boolean eliminar(Integer id);
}
